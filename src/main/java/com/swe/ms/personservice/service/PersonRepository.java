package com.swe.ms.personservice.service;

import com.swe.ms.personservice.domain.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
}
