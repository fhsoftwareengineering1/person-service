package com.swe.ms.personservice.service;

import com.swe.ms.personservice.exception.PersonNotFoundExecption;
import com.swe.ms.personservice.resources.PersonResource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
@Slf4j
public class PersonService {

    private final PersonRepository personRepository;
    private final PersonMapper personMapper;

    public List<PersonResource> findAll() {
        log.info("loading all persons {}");
        return StreamSupport.stream(personRepository.findAll().spliterator(), false)
                            .map(personMapper::mapFromEntity)
                            .collect(Collectors.toList());
    }

    public PersonResource addPerson(PersonResource personResource) {
        log.info("saving person {}", personResource);
        return personMapper.mapFromEntity(
                personRepository.save(
                        personMapper.mapToEntity(personResource)
                ));
    }

    public void updatePerson(Long id, PersonResource personResource) {
        log.info("updating provided person {} with id {}", personResource, id);
        personRepository.findById(id)
                        .ifPresentOrElse(person -> personRepository.save(personMapper.mapToUpdateableEntity(person.getId(), personResource)),
                                () -> {
                                    throw new PersonNotFoundExecption("Person with provided ID not found!");
                                });
    }

    public void deletePerson(final Long id) {
        log.info("deleting person with id {}", id);
        personRepository.findById(id)
                        .ifPresentOrElse(person -> personRepository.deleteById(person.getId()),
                                () -> {
                                    throw new PersonNotFoundExecption("Person with provided ID not found!");
                                });
    }

    public PersonResource findById(Long id) {
        log.info("searching for person with id {}", id);
        return personMapper.mapFromEntity(
                personRepository.findById(id)
                                .orElseThrow(() -> new PersonNotFoundExecption("Person with provided ID not found!"))
        );
    }
}
