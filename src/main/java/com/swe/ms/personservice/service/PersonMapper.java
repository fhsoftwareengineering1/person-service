package com.swe.ms.personservice.service;

import com.swe.ms.personservice.domain.Person;
import com.swe.ms.personservice.resources.PersonResource;
import org.springframework.stereotype.Component;

@Component
public class PersonMapper {

    PersonResource mapFromEntity(Person person) {
        return PersonResource.builder()
                             .id(person.getId())
                             .firstname(person.getFirstname())
                             .lastname(person.getLastname())
                             .email(person.getEmail())
                             .birthday(person.getBirthday())
                             .build();
    }

    Person mapToEntity(PersonResource personResource) {
        return Person.builder()
                     .firstname(personResource.getFirstname())
                     .lastname(personResource.getLastname())
                     .email(personResource.getEmail())
                     .birthday(personResource.getBirthday())
                     .build();
    }

    Person mapToUpdateableEntity(Long id, PersonResource personResource) {
        return Person.builder()
                     .id(id)
                     .firstname(personResource.getFirstname())
                     .lastname(personResource.getLastname())
                     .email(personResource.getEmail())
                     .birthday(personResource.getBirthday())
                     .build();
    }
}
