package com.swe.ms.personservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PersonNotFoundExecption extends RuntimeException {

    public PersonNotFoundExecption(String message) {
        super(message);
    }

}
