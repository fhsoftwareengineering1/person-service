package com.swe.ms.personservice.resources;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDate;

@Builder
@Getter
@ToString
public class PersonResource {

    @JsonIgnore
    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    private LocalDate birthday;
}
