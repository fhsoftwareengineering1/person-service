CREATE TABLE IF NOT EXISTS persons (

    id BIGSERIAL PRIMARY KEY,
    firstname varchar(50),
    lastname varchar(50),
    email varchar(50),
    birthday date
);