# Person Service
Application to execute CRUD Operations on a 'Person-Domain'


## Build application
* Navigate to project-service/
* execute ```./mvnw clean install```
* To start the application execute ```java -jar -Dspring.profiles.active=development target/project-service-1.jar```

## Build Docker-Image
* requires local [Docker Environment](https://docs.docker.com/)
* navigate to project-service/
* execute ```./mvnw spring-boot:build-image```

## Start application with development PostgreSQL database & load-balancing nginx
* install [docker-compose](https://docs.docker.com/compose/)
* navigate to project-service/
* run ```docker-compose up```


### Sample Requests with ```curl```
* Load All Persons: 
```curl -X GET http://localhost:80/api/persons```

* Load Person by ID
```curl -X GET http://localhost:80/api/persons/{personId}```

* Add Person
```curl -H 'Content-Type: application/json' -X POST -d '{"firstname":"Testperson","lastname":"Testperson","email":"mail@testperson.at","birthday": "2000-01-30"}' http://localhost:80/api/persons```

* Update Person
```curl -H 'Content-Type: application/json' -X PUT -d '{"firstname":"Testperson","lastname":"Testperson","email":"mail@testperson.at","birthday": "2000-01-30"}' http://localhost:80/api/persons/{personId}```

* Delete Person
```curl -X DELETE http://localhost:80/api/persons/{personId}```

### Sample Requests with Postman Collection 
```project-service/Person-Service.postman_collection.json``` 