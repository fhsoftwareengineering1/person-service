#I. Codebase
Eine im Versionsmanagementsystem verwaltete Codebase -
in diesem Fall ```Gitlab``` - welche für verschiedene Stages genutzt werden kann.
#II. Abhängigkeiten
Abhängigkeiten explizit mittels [Apache Maven](https://maven.apache.org/) deklariert.
Diese werden (bei Bedarf/ je nach Konfiguration) mitgepackaged.
#III. Konfiguration
Die Konfiguration wird in Umgebungsvariablen abgelegt.
Steuerbar via Configuration Files ```application-stageX.yml``` per Stage.
Zusätzliche Möglichkeit zum Überschreiben der Konfiguration via Environmentvariablen, beim Start der Applikation
#IV. Unterstützende Dienste
Unterstützende Dienste als angehängte Ressourcen behandelt: z.B.: Postgres Database als Konfiguration
#V. Build, release, run
Build- und Run-Phase strikt getrennt: 
So kann während des Build Zeitpunkts (z.B.: In einer Pipeline) die Applikation gebaut/gepackaged 
und das Docker Image erstellt (und dieses ggf. in ein "Docker"-Repository gepublished) werden.
Dieses kann im Deploy/Release Step gepullt und gestartet werden.
#VI. Prozesse
Die App als einen oder mehrere Prozesse ausführen: Siehe ```docker-compose.yml```
Die Applikation ist ```stateless``` und kann dadurch als mehrere Replicas deployed werden. 
Das Loadbalancing der Replicas übernimmt ein dementsprechend konfigurierter nginx. 
#VII. Bindung an Ports
Dienste durch das Binden von Ports exportieren:
Docker-Portbinding im Dockerfile, aber auch extern via ```docker-compose.yml```
#VIII. Nebenläufigkeit
Mit dem Prozess-Modell skalieren:
Siehe ```docker-compose.yml```
Die Applikation ist Stateless und kann dadurch als mehrere Replicas deployed werden. 
Das Loadbalancing der Replicas übernimmt ein dementsprechend konfigurierter nginx.
#IX. Einweggebrauch
Robuster mit schnellem Start und problemlosen Stopp.
Schneller(er) Start durch z.B.: Spring Boot lazy-initialization.
Durch Docker ```sigterm``` graceful shutdown der Spring-Boot-Applikation. 
Nach konfigurierbarer Zeit mögliches ```sigkill``` zum sofortigen Beenden des Prozesses.
#X. Dev-Prod-Vergleichbarkeit
Entwicklung, Staging und Produktion so ähnlich wie möglich gehalten.
Unterschiede werden durch Stage-Konfiguration oder Environment Variablen gehalten.
#XI. Logs
Logs werden, als Event, auf ```STDOUT``` geschrieben. 
Diese können in der Runtime Infrastructure von ```AWS Cloudwatch/Filebeat``` etc. gesammelt werden.
Die Darstellung der Logs kann demnach in selektiven Tools z.B.: ```Kibana``` erfolgen.
#XII. Admin-Prozesse
Database Setup mit [Flyway](https://flywaydb.org/)